FROM alpine:3.20.3

RUN apk add --no-cache janus-gateway && \
    ls /etc/janus/*.sample | while read JCFG; do mv "$JCFG" "${JCFG%.sample}"; done

ENTRYPOINT ["janus"]